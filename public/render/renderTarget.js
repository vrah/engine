import { Scene } from './objects/Scene.js';
import { Camera } from './camera.js';
import { Rect } from './objects/primitive.js';
import { gl } from './core.js';

import { BatchManager } from './batchManager.js';

export class RenderTarget
{
    _scene = new Scene();
    _camera = new Camera();
    _viewPort = new Rect();
    _batchManager = new BatchManager();
    _arrayNodes = [];
   first = false

   constructor(viewPort = new Rect(100, 100, 800, 600))
   {
       // super(props);

       this._viewPort = viewPort;
       this._scene.matrix = this._camera.uProjectionViewMatrix;
   }

   setCameraEventToDiv(div)
   {
       div.onmousemove = this._camera.handleMouseMove.bind(this._camera);
       div.onmousedown = this._camera.handleMouseDown.bind(this._camera);
       div.onmouseup = this._camera.handleMouseUp.bind(this._camera);
       div.onwheel = this._camera.handleMouseWheel.bind(this._camera);
   }
   draw()
   {
       gl.clearColor(0.8, 0.8, 0.8, 1.0);
       // Clear the color buffer with specified clear color
       gl.clearDepth(1.0); // Clear everything
       gl.enable(gl.DEPTH_TEST); // Enable depth testing
       gl.depthFunc(gl.LEQUAL); // Near things obscure far things
       gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

       // if(!this.first) {
       //     this.first = true
       const array = this.nodesFlat;

       this._batchManager.clearAll();
       this._batchManager.registeredNodesArray(array);

       // }

       this._batchManager.draw();
   }

   get scene()
   {
       return this._scene;
   }

   getNodesFlat(node, array)
   {
       node.updateMatrix();
       array[this._index++] = node;

       // array.push(node);

       // node.updateMatrix();
       const children = node.children;
       const childrenCount = children.length;
       let indexChildren = 0;

       while (indexChildren < childrenCount)
       {
           this.getNodesFlat(children[indexChildren++], array);
       }
   }

   get nodesFlat()
   {
       this._index = 0;
       this._arrayNodes = [];
       this.getNodesFlat(this._scene, this._arrayNodes);
       // this._arrayNodes.reverse();
       return this._arrayNodes;
   }
}
export class RenderTargetTexture extends RenderTarget
{

}
