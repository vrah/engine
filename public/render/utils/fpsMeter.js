export class FpsMeter
{
    _stackFpsMeterMax = 60;
    _stackFpsMeterMultiply = 1 / this._stackFpsMeterMax;
    _stackFpsMeter = 0;
    _stackFpsMeterCount = 0;
    _currentFps = 0;
    _delta = 0;
    _max = 10;
    _min = 10000;
    timeNow;
    lastTime;
    startTime;
    constructor(p_div)
    {
        this.initConteinerStyle(p_div);
        this.timeNow = this.lastTime = this.startTime = Date.now();
    }

    initConteinerStyle(p_div)
    {
        this.fps_ = document.createElement('div');
        this.fps_.style.zIndex = 9999999999;

        this.fps_.style.position = 'fixed';
        this.fps_.style.bottom = `${0}px`;
        this.fps_.style.color = '#000';
        p_div.appendChild(this.fps_);
    }

    load(value_)
    {
        this.fps_.innerHTML = `LOADING : ${value_} %`;
    }

    setText(text_)
    {
        this._text = text_;
    }
    get delta()
    {
        return this._delta;
    }

    get currentFps (){
        return this._currentFps;
    }

    draw(text_ = this._text)
    {
        this.timeNow = Date.now(); // cas nyni
        this._delta = this.timeNow - this.lastTime;
        this._stackFpsMeter += this._delta;
        this._stackFpsMeterCount++;
        if ((this._stackFpsMeterCount % this._stackFpsMeterMax) === 0)
        {
            this._currentFps = 1000 / (this._stackFpsMeter * this._stackFpsMeterMultiply);
            this._currentFps = this._currentFps.toFixed(1);
            const delta = (this._stackFpsMeter * this._stackFpsMeterMultiply).toFixed(1);

            this._stackFpsMeter = 0;
            this._stackFpsMeterCount = 0;

            this._max = this._max > this._currentFps ? this._max : this._currentFps;
            this._min = this._min < this._currentFps ? this._min : this._currentFps;
            this.fps_.innerHTML = ` ${this._currentFps} ${this._min} ${this._max}<br/>${delta} <br/>${text_}`;
        }

        this.lastTime = this.timeNow; // cas nyni
    }
}
