const privateMethod = Symbol('privateMethod');
const COUNT = 100000000;
let lastTime = Date.now();
export let start = () =>{
    lastTime = Date.now();
}
export let end = (text, show = true) =>{
    if(show) {
        console.log(text, 'time: ', Date.now() - lastTime)
    }
}
//
//
//
//
// let testPerformance = (fnc, text = "no ame", count = COUNT) => {
//
//     let time = Date.now();
//     time = Date.now();
//
//     for (let i = 0; i < count; i++) {
//         // fnc();
//         testPerformanceCallES5(fnc);
//     }
//
//     console.log(text + 'es5', Date.now() - time)
//     time = Date.now();
//     for (let i = 0; i < count; i++) {
//         // fnc();
//         testPerformanceCallArrow(fnc);
//     }
//
//     console.log(text + 'arrow', Date.now() - time)
// }
//
// let testPerformanceCallES5 = function(fnc) {
//     fnc();
// }
//
// let testPerformanceCallArrow = (fnc) =>{
//     fnc();
// }
//
// class Size {
//     set width(value) {
//         this.#width = value;
//     }
//
//     set height(value) {
//         this.#height = value;
//     }
//
//     get width() {
//         return this.#width;
//     }
//
//     get height() {
//         return this.#height;
//     }
//
//     [privateMethod](value) {
//         this.#height = value;
//         return this.#height * this.#width;
//     }
//
//     publicMethod(value) {
//         this.#height = value;
//         return this.#height * this.#width;
//     }
//
//     #private = function (value) {
//         this.#height = value;
//         return this.#height * this.#width;
//     }
//
//     #privateArrow = (value) => {
//         this.#height = value;
//         return this.#height * this.#width;
//     }
//     //cannot use
//     // #privateM3(value) {
//     //     this.#height = value;
//     //     return  this.#height * this.#width;
//     // }
// // for test
//     _width = 0;
//     _height = 0;
//
//     // private
//     #width = 0;
//     #height = 0;
//
//     test() {
//         testPerformance(() => {
//             this[privateMethod](20)
//         }, `privateMethod Symbol`);
//
//         testPerformance(() => {
//             this.#privateM(20)
//         }, `private anonym function`);
//
//         testPerformance(() => {
//             this.#privateM2(20)
//         }, `private arrow`);
//
//
//         testPerformance(() => {
//             this.publicMethod(20)
//         }, `publicMethod`);
//
//     }
//
//     constructor(width, height) {
//         this._width = width;
//         this._height = height;
//
//         this.#width = width;
//         this.#height = height;
//
//
//     }
//
//
// }
//
// function multipleMethod(size) {
//     size.width * size.height + size.height
//     size.width = 20
// }
//
// function multipleParam(size) {
//     size._width * size._height + size._height
//     size._width = 20
// }
//
// function test() {
//
//     let testObj = new Size(20, 20);
//     testPerformance(() => {
//         multipleParam(testObj)
//     }, `public params  `);
//
//
//     testPerformance(() => {
//         multipleMethod(testObj)
//     }, `getter setter`);
//
//     let testobj2 = new SizeES5(20, 20);
//     testobj2.test()
//     testPerformance(() => {
//         multipleParam(testobj2)
//     }, `para es5`);
//
//
//     testObj.test()
// }
//
// function SizeES5(width, height) {
//     this._width = width;
//     this._height = height;
// }
//
// SizeES5.prototype.publicMethod = function (value) {
//     this._height = value;
//     return this._height * this._width;
// }
//
//
// SizeES5.prototype.test = function () {
//     testPerformance(() => {
//         this.publicMethod(20)
//     }, `publicMethod es5 this`);
//
// }
// // test()
// // export default test;
// // export default 5