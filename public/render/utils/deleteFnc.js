
// drawNodeRecursion(node)
// {
//     node.updateMatrix();
//     node.draw();
//     const children = node.children;
//     const childrenCount = children.length;
//     let index = 0;
//
//     while (index < childrenCount)
//     {
//         this.drawNodeRecursion(children[index++]);
//     }
// }
//
// drawNodeIteration(node)
// {
//     let deep = 0;
//     const elements = [node];
//     const deepsIndex = [0];
//     const deepsChildren = [elements[deep].children];
//     let currentIndex;
//     let child;
//
//     while (deep >= 0)
//     {
//         currentIndex = deepsIndex[deep];
//         child = deepsChildren[deep][currentIndex];
//
//         if (!child)
//         {
//             deep--;
//             continue;
//         }
//
//         child.draw();
//         deepsIndex[deep]++;
//         if (child.children.length)
//         {
//             deep++;
//             deepsIndex[deep] = 0;
//             deepsChildren[deep] = child.children;
//
//             continue;
//         }
//     }
// }