import { core, textureManager } from './core.js';
import { SpriteBatch } from './objects/index.js';
export class BatchManager
{
    _batchNodes = {};
    _normalNodes =[];
    constructor()
    {

    }
    registered(node)
    {
        const textureId = node._textureId;

        if (!textureId)
        {
            this._normalNodes.push(node);

            return;
        }

        if (!this._batchNodes[textureId])
        {
            this._batchNodes[textureId] = new SpriteBatch();
        }
        this._batchNodes[textureId].registered(node);
    }
    clearAll()
    {
        this._batchNodes = {};
        this._normalNodes = [];
    }

    registeredNodesArray(arrayNodes)
    {
        for (let i = 0; i < arrayNodes.length; i++)
        {
            const node = arrayNodes[i];

            this.registered(node);
        }
    }
    draw()
    {
        for (const id in this._batchNodes)
        {
            const batchNode = this._batchNodes[id];

            batchNode.setBuffers();
            batchNode.draw();
        }

        for (const id in this._normalNodes)
        {
            const node = this._normalNodes[id];

            node.setBuffers();
            node.draw();
        }
    }
}
