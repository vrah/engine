import { Node } from './../objects/node.js';
import { gl, shaderManager, textureManager } from './../core.js';
import { mat4, vec2, vec3, vec4 } from './../third_party/glMatrix/index.js';
import { Rect, Size } from './primitive.js';

export class Sprite extends Node
{
    // private only in chrome https://v8.dev/features/class-fields
    _vertexBuffer;
    _textureId = '';
    _texture = null;
    _shader = null;
    _anchorPoint = vec2.create();
    _texturePosition = vec3.create();
    _rect = new Rect();
    _size = new Size();

    _vertexCountOne = 4;
    _vertexCount = this._vertexCountOne;
    _vertexPositionOne = 4; // vec4
    _vertexUVsOne = 2; // vec2
    _vertexIndexMatrixOne = 1;
    _vertexSizeOne = this._vertexUVsOne + this._vertexPositionOne + this._vertexIndexMatrixOne;
    _vertexPositionSize = this._vertexPositionOne * 4;
    _vertexUVsSize = this._vertexUVsOne * 4;
    _vertexIndexMatrixSize = this._vertexIndexMatrixOne * 4;
    _vertexStride = this._vertexPositionSize + this._vertexUVsSize + this._vertexIndexMatrixSize;
    _vertexPositionOffset = 0;
    _vertexUVOffset = this._vertexPositionOne * 4;
    // ehm
    _vertexIndexMatrixOffset = this._vertexUVOffset + this._vertexPositionOne * 2;
    _vertexSize = this._vertexCount * this._vertexSizeOne;
    _vertex = new Float32Array(this._vertexSize);
    _vertexSizeMatrix = this._vertexCount * 16;
    _vertexMatrixBuffer;
    _vertexMatrix = new Float32Array(this._vertexSizeMatrix);
    _pointsPosition = [
        new Float32Array(4),
        new Float32Array(4),
        new Float32Array(4),
        new Float32Array(4),
    ];
    _pointsUV = [
        new Float32Array(2),
        new Float32Array(2),
        new Float32Array(2),
        new Float32Array(2),

    ];

    _indicesCountOne = 6;
    _indicesCount = this._indicesCountOne;
    _indices = new Uint16Array([0, 1, 2, 1, 3, 2]);
    _indicesBuffer;

    // https://esdiscuss.org/topic/async-class#content-31
    constructor(spriteName)
    {
        super();
        this.initShader();
        this.initBuffers();
        this.initTexture(spriteName);
        // this.setBuffers();
    }

    initShader()
    {
        this._shader = shaderManager.getShader(
            './render/shaders/normal/normalVS.glsl',
            './render/shaders/normal/normalFS.glsl');
    }
    static get INDICATE_PATTERN()
    {
        return [0, 1, 2, 1, 3, 2];
    }

    updateMatrix()
    {
        mat4.identity(this._matrix);
        mat4.translate(this._matrix, this._matrix, this._texturePosition);
        mat4.translate(this._matrix, this._matrix, this._position);
        mat4.scale(this._matrix, this._matrix, this._scale);

        if (this._parent)
        {
            mat4.multiply(this._matrix, this._parent.matrix, this._matrix);
        }
    }

    // -TEXTURE

    set textureName (spriteName) {
        this.texture = textureManager.getTexture(spriteName);
    }
    /**
     * Seter metho d for set teture
     * @setter
     * @param texture
     */
    set texture(texture)
    {
        this._texture = texture;
        this._textureId = this._texture.name;

        this._rect = this._texture._rect;
        this._rectUV = this._texture.rectUV;
        this._pointsPosition = this._rect.points;
        this._pointsUV = this._rectUV.points;

        this._texturePosition = vec3.clone([
            this._rect.width * this._anchorPoint[0],
            this._rect.height * this._anchorPoint[1],
            0]);
    }

    initTexture(spriteName)
    {
        this.texture = textureManager.getTexture(spriteName);
    }

    drawTexture()
    {
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, this._texture.texture);
        gl.uniform1i(this._shader.locationList.uSampler.param, 0);
    }
    // END-TEXTURE

    drawAttribute()
    {
        const location = this._shader.locationList;

        gl.bindBuffer(gl.ARRAY_BUFFER, this._vertexBuffer);
        gl.vertexAttribPointer(location.aVertexPosition.param, this._vertexPositionOne, gl.FLOAT, false, this._vertexStride, this._vertexPositionOffset);
        gl.enableVertexAttribArray(location.aVertexPosition.param);

        gl.vertexAttribPointer(location.aTextureCoord.param, this._vertexUVsOne, gl.FLOAT, false, this._vertexStride, this._vertexUVOffset);
        gl.enableVertexAttribArray(location.aTextureCoord.param);

        gl.vertexAttribPointer(location.aIndicateMatrix.param, this._vertexIndexMatrixOne, gl.FLOAT, false, this._vertexStride, this._vertexIndexMatrixOffset);
        gl.enableVertexAttribArray(location.aIndicateMatrix.param);
    }

    createBuffer()
    {
        return gl.createBuffer();
    }

    initBuffers()
    {
        this._vertexBuffer = this.createBuffer();
        this._vertexMatrixBuffer = this.createBuffer();
        this._indicesBuffer = this.createBuffer();
    }

    /**
     *
     * @param buffer
     * @param data
     * @param type
     * @param usage
     */
    setBuffer(buffer, data, target = gl.ARRAY_BUFFER, usage = gl.DYNAMIC_DRAW)
    {
        gl.bindBuffer(target, buffer);
        gl.bufferData(target,
            data,
            // new Float32Array(data),
            usage);
    }

    setBuffers()
    {
        this.setBufferAll(this._vertex);
        this.setBuffer(this._vertexBuffer, this._vertex);
        this.setBuffer(this._indicesBuffer, this._indices, gl.ELEMENT_ARRAY_BUFFER);
    }

    setBufferAll(buffer, index = 0)
    {
        // vec4.transformMat4(this._pointsPosition[0], positionPoint[0], this._matrix);
        // vec4.transformMat4(this._pointsPosition[1], positionPoint[1], this._matrix);
        // vec4.transformMat4(this._pointsPosition[2], positionPoint[2], this._matrix);
        // vec4.transformMat4(this._pointsPosition[3], positionPoint[3], this._matrix);

        for (let i = 0; i < this._vertexCountOne; i++)
        {
            const indexVertex = (this._vertexSize * index) + ((this._vertexSizeOne) * i);

            buffer[indexVertex] = this._pointsPosition[i][0];
            buffer[indexVertex + 1] = this._pointsPosition[i][1];
            buffer[indexVertex + 2] = this._pointsPosition[i][2];
            buffer[indexVertex + 3] = this._pointsPosition[i][3];
            buffer[indexVertex + 4] = this._pointsUV[i][0];
            buffer[indexVertex + 5] = this._pointsUV[i][1];
            buffer[indexVertex + 6] = index;
        }
    }

    setUniformArrayMatrix()
    {
        const location = this._shader.locationList;

        gl.uniformMatrix4fv(location.uMatrices.param[0], false, this.matrix);
    }

    draw()
    {
        gl.useProgram(this._shader.program);
        // gl.disable(gl.DEPTH_TEST);
        gl.enable(gl.BLEND);
        gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);
        // gl.blendFunc(gl.SRC_ALPHA, gl.ONE);
        // gl.enable(gl.BLEND);
        // gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);
        gl.enable(gl.CULL_FACE);
        gl.cullFace(gl.FRONT);
        // Set the shader uniforms
        this.drawTexture();
        this.drawAttribute();
        this.setUniformArrayMatrix();

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this._indicesBuffer);
        gl.drawElements(gl.TRIANGLES, this._indicesCount, gl.UNSIGNED_SHORT, 0);
    }
}
