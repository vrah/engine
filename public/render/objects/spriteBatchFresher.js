import { Sprite } from './index.js';
import { gl, shaderManager } from './../core.js';
import { ArrayBufferFull, ItemData, ArrayBufferDrawElement } from '../buffers/index.js';

export class SpriteBatch extends Sprite
{
    _nodes = [];
    _index = 0;
    // _indicesBuffer = new ArrayBufferDrawElement();

    initShader()
    {
        this._shader = shaderManager.getShader(
            './render/shaders/batch/normalVS.glsl',
            './render/shaders/batch/normalFS.glsl');
    }

    initTexture()//u1646 288
    {
    }

    updateMatrix()
    {
    }




    clearAll()
    {
        this._index = 0;
        this._nodes = [];
    }

    registered(node)
    {
        this._nodes[this._index++] = node;
        if (!this._texture)
        {
            this._texture = node._texture;
        }
    }
    _vertexCountOne = 4;
    _indicesCountOne = 6;
    generateIndicates(count = 1)
    {
        this._indices = new Uint16Array(count * this._indicesCountOne);
        const pattern = Sprite.INDICATE_PATTERN;

        for (let i = 0; i < count; i++) {
            for (let j = 0; j < this._indicesCountOne; j++) {
                const index = (i * this._indicesCountOne) + j;
                const offset = i * this._vertexCountOne;

                this._indices[index] = pattern[j] + offset;
            }
        }


    }

    setUniformArrayMatrix()
    {
        const params = this._shader.locationList.uMatrices.param;

        for (let i = 0, len = this._nodes.length; i < len; i++)
        {
            // const matrix = this._nodes[i].matrix;
            gl.uniformMatrix4fv(params[i], false, this._nodes[i].matrix);
        }
    }

    setBuffers()
    {
        this._vertexCount = this._nodes.length * this._vertexCountOne;
        // this._vertex = new Float32Array(this._vertexCount * this._vertexSizeOne);

        this._vertexBuffer.countObject = this._nodes.length;
        for (let i = 0; i < this._nodes.length; i++)
        {
            const node = this._nodes[i];
            this._vertexBuffer.setBufferUpdate({
                position: node._pointsPosition,
                uv: node._pointsUV,
                indexMatrix: [
                    new Float32Array([i]),
                    new Float32Array([i]),
                    new Float32Array([i]),
                    new Float32Array([i]),
                ]
            }, i);
            // node.setBufferUpdate(i, );
        }
        // this.setBuffer(this._vertexBuffer, this._vertex);

        this._indicesCount = this._nodes.length * this._indicesCountOne;
        this.generateIndicates(this._nodes.length);
        this._indicesBuffer.setBufferUpdate(this._indices);
    }

}
