import {mat4, vec3} from './../third_party/glMatrix/index.js'
import {animator} from "./../core.js";

let g_count_node = 0;

export class Node {
    //private only in chrome https://v8.dev/features/class-fields
    //https://www.chromestatus.com/feature/6035156464828416
    _matrix = new mat4.create();
    _position = new vec3.create();
    _scale = new vec3.clone([1, 1, 1]);
    _skew = new vec3.create();
    _children = [];
    _zIndex = 0;
    _parent = null;
    _animator = animator;
    _id = g_count_node++;

    draw() {
        // this.updateMatrix()
    }

    constructor() {
    }

    init() {
    }

    setScene() {

    }

    updateMatrix() {
        this._matrix = new mat4.create();
        mat4.translate(this._matrix, this._matrix, this._position)
        mat4.scale(this._matrix, this._matrix, this._scale)

        if (this._parent) {
            mat4.multiply(this._matrix, this._parent.matrix, this._matrix);
        }
    }

    //animation
    runAnimation(animation_) {
        animation_.node = this;
        this._animator.addAnimation(animation_);
    }

    clearAllAnimation() {
        // console.log(this._id_animation_sprite_anim);
        this._animator.clearAnimationByNode(this);
    }

    clearAnimationById(id_) {
        // console.log(this._id_animation_sprite_anim);
        this._animator.clearAnimationById(id_);
    }

    //--end animation


    get id() {
        return this._id;
    }

    set id(id) {
        this._id = id;
    }

    get zIndex() {
        return this._zIndex
    }

    set zIndex(zIndex) {
        this._zIndex = zIndex
    }

    addChild(node, zIndex = this._children.length) {
        this.addChildAt(node, zIndex);
    }

    addChildAt(node, zIndex) {
        node.parent = this;
        node.zIndex = zIndex;
        this._children.splice(zIndex, 1, node);
        // this._children.push(node);
        this._children.sort((a, b) => {
            return a.zIndex - b.zIndex;
        })
    }

    set parent(parent) {
        this._parent = parent;
    }

    get parent() {
        return this._parent
    }

    get children() {
        return this._children;
    }

    set position(value) {
        this._position = value;
    }

    set scale(value) {
        this._scale = value;
    }

    set skew(value) {
        this._skew = value;
    }

    set matrix(value) {
        this._matrix = value;
    }
    get matrix() {
        return this._matrix;
    }

    get position() {
        return this._position;
    }

    get scale() {
        return this._scale;
    }

    get skew() {
        return this._skew;
    }
}
