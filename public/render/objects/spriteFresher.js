import { Node } from './../objects/node.js';
import { gl, shaderManager, textureManager } from './../core.js';
import { mat4, vec2, vec3, vec4 } from './../third_party/glMatrix/index.js';
import { Rect, Size } from './primitive.js';
import { ArrayBufferFull, ItemData, ArrayBufferDrawElement } from '../buffers/index.js';

export class Sprite extends Node
{
    // private only in chrome https://v8.dev/features/class-fields
    _vertexBuffer;
    _textureId = '';
    _texture = null;
    _shader = null;
    _anchorPoint = vec2.create();
    _texturePosition = vec3.create();
    _rect = new Rect();
    _size = new Size();

    _pointsPosition = [
        new Float32Array(4),
        new Float32Array(4),
        new Float32Array(4),
        new Float32Array(4),
    ];
    _pointsUV = [
        new Float32Array(2),
        new Float32Array(2),
        new Float32Array(2),
        new Float32Array(2),
    ];
    _indexesMatrix = [
        new Float32Array([0]),
        new Float32Array([0]),
        new Float32Array([0]),
        new Float32Array([0]),
    ]
    _pointsColor = [
        new Float32Array([0, 1, 1, 1]),
        new Float32Array([1, 0, 1, 1]),
        new Float32Array([1, 1, 0, 1]),
        new Float32Array([1, 1, 1, 1]),
    ]
    _indices = new Uint16Array([0, 1, 2, 1, 3, 2]);
    _indicesBuffer;

    // https://esdiscuss.org/topic/async-class#content-31
    constructor(spriteName)
    {
        super();
        this.initShader();
        this.initBuffers();
        this.initTexture(spriteName);
    }

    initShader()
    {
        this._shader = shaderManager.getShader(
            './render/shaders/normal/normalVS.glsl',
            './render/shaders/normal/normalFS.glsl');
    }
    static get INDICATE_PATTERN()
    {
        return [0, 1, 2, 1, 3, 2];
    }

    updateMatrix()
    {
        mat4.identity(this._matrix);
        mat4.translate(this._matrix, this._matrix, this._texturePosition);
        mat4.translate(this._matrix, this._matrix, this._position);
        mat4.scale(this._matrix, this._matrix, this._scale);

        if (this._parent)
        {
            mat4.multiply(this._matrix, this._parent.matrix, this._matrix);
        }
    }

    // -TEXTURE

    set textureName(spriteName)
    {
        this.texture = textureManager.getTexture(spriteName);
    }
    /**
     * Seter metho d for set teture
     * @setter
     * @param texture
     */
    set texture(texture)
    {
        this._texture = texture;
        this._textureId = this._texture.name;

        this._rect = this._texture._rect;
        this._rectUV = this._texture.rectUV;
        this._pointsPosition = this._rect.points;
        this._pointsUV = this._rectUV.points;

        this._texturePosition = vec3.clone([
            this._rect.width * this._anchorPoint[0],
            this._rect.height * this._anchorPoint[1],
            0]);

        this._vertexBuffer.setBufferUpdate({
            position: this._pointsPosition,
            uv: this._pointsUV,
        });
    }

    initTexture(spriteName)
    {
        this.texture = textureManager.getTexture(spriteName);
    }

    drawTexture()
    {
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, this._texture.texture);
        gl.uniform1i(this._shader.locationList.uSampler.param, 0);
    }
    // END-TEXTURE

    initBuffers()
    {
        const location = this._shader.locationList;

        this.__position = new ItemData(this._pointsPosition, location.aVertexPosition.param);
        this.__uv = new ItemData(this._pointsUV, location.aTextureCoord.param);
        this.__indexMatrix = new ItemData(this._indexesMatrix, location.aIndicateMatrix.param);
        // this.__color = new ItemData(this._pointsColor, location.aColor.param);

        this._vertexBuffer = new ArrayBufferFull({
            position: this.__position,
            uv: this.__uv,
            indexMatrix: this.__indexMatrix,
            // color: this.__color
        });

        this._indicesBuffer = new ArrayBufferDrawElement();
        this._indicesBuffer.setBufferUpdate(this._indices);
    }

    setBuffers()
    {
        // this._vertexBuffer.setBuffer([this._pointsPosition, this._pointsUV, this._indexesMatrix])
        // this._indicesBuffer.setBuffer(this._indices)
        // this.setBuffer(this._indicesBuffer, this._indices, gl.ELEMENT_ARRAY_BUFFER);
    }

    setUniformArrayMatrix()
    {
        const location = this._shader.locationList;

        gl.uniformMatrix4fv(location.uMatrices.param[0], false, this.matrix);
    }

    draw()
    {
        gl.useProgram(this._shader.program);
        // gl.disable(gl.DEPTH_TEST);
        gl.enable(gl.BLEND);
        gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);
        // gl.blendFunc(gl.SRC_ALPHA, gl.ONE);
        // gl.enable(gl.BLEND);
        // gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);
        gl.enable(gl.CULL_FACE);
        gl.cullFace(gl.FRONT);
        // Set the shader uniforms
        this.drawTexture();
        this._vertexBuffer.drawBuffer();

        this.setUniformArrayMatrix();
        this._indicesBuffer.drawBuffer();
    }
}
