import { Sprite } from './index.js';
import { gl } from './../core.js';
;
import { TextureImage } from '../textures/textureImage.js';

export class Text extends Sprite
{
    _fontName= `Arial`;
    _fontSize= 40;
    _font = `${this._fontSize}px ${this._fontName}`;

    // // private only in chrome https://v8.dev/features/class-fields
    constructor(props)
    {
        super(props);
    }


    setFont() {
        this._fontName= `Arial`;
        this._fontSize= 40;
        this._font = `${this._fontSize}px ${this._fontName}`;
    }
    initTexture(text)
    {
        this.setFont();
        this.createCanvas();
        this.setString(text);
        this.setTexture();
        // //
        // this.canvas.style.border = "thick solid #0000FF";
        // window.document.body.append(this.canvas)
    }

    createCanvas()
    {
        // look up the text canvas.
        this.canvas = document.createElement('canvas');
        // make a 2D context for it
        this.ctx = this.canvas.getContext('2d');
    }

    setString(text = 'not set')
    {
        const textToWrite = text;
        // const textSize = 40;

        this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);

        // this.ctx.beginPath();
        this.ctx.textAlign = 'left';	// This determines the alignment of text, e.g. left, center, right
        this.ctx.textBaseline = 'bottom';

        this.ctx.font = this._font; 	// Set the font of the text before measuring the width!
        this.canvas.width = getPowerOfTwo(this.ctx.measureText(textToWrite).width);
        this.canvas.height = getPowerOfTwo(2 * this._fontSize);
        // this.ctx.beginPath();
        this.ctx.textAlign = 'left';	// This determines the alignment of text, e.g. left, center, right
        this.ctx.textBaseline = 'hanging';
        this.ctx.font = this._font; 	// Set the font of the text before measuring the width!
        this.ctx.strokeStyle = 'white';
        this.ctx.lineWidth = 6;
        const x = 0;
        const y = this.canvas.height - this._fontSize - 10;

        this.ctx.strokeText(textToWrite, x, y);
        this.ctx.fillText(textToWrite, x, y);
        this._rect.width = this.canvas.width;
        this._rect.height = this.canvas.width;

        this.setTexture();
    }

    setTexture()
    {
        this.texture = new TextureImage(this.canvas, gl);
        this.setBuffers();
    }
}

function getPowerOfTwo(value, pow = 1)
{
    while (pow < value)
    {
        pow *= 2;
    }

    return pow;
}
