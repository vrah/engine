export class Size {
    set width(value) {
        this._width = value;
    }

    set height(value) {
        this._height = value;
    }

    get width() {
        return this._width;
    }

    get height() {
        return this._height;
    }

    //private only in chrome https://v8.dev/features/class-fields
    // private
    _width = 0;
    _height = 0;
    _points = [];

    constructor(width, height) {
        this._width = width;
        this._height = height;

        this._width = width;
        this._height = height;

        this._points[0] = new Float32Array([0, 0, 0, 1]);
        this._points[1] = new Float32Array([0, this._height, 0, 1]);
        this._points[2] = new Float32Array( [this._width, 0, 0, 1]);
        this._points[3] = new Float32Array([this._width, this._height, 0, 1]);
    }

    get points() {
        return this._points;
    }

    get pointsClone() {

        this._points[0] = [0, 0, 0, 1];
        this._points[1] = [0, this._height, 0, 1];
        this._points[2] = [this._width, 0, 0, 1];
        this._points[3] = [this._width, this._height, 0, 1];
        return this._points;


        // this._points[1][1] = this._height;
        // this._points[2][0] = this._width;
        // this._points[3][0] = this._width;
        // this._points[3][0] = this._height;
        // return this._points;


        // let points = [];
        // for (let i = 0; i < 4; i++) {
        //     points[i] = []
        //     for (let j = 0; j < 4; j++) {
        //         points[i][j] = this._points[i][j]
        //     }
        // }
        // return points;

        // return JSON.parse(JSON.stringify(this._points));
    }
}


export class Point {
    _x = 0;
    _y = 0;

    constructor(x_ = this._x, y_ = this._y) {
        this._x = x_ * 1;
        this._y = y_ * 1;
    }


    get x() {
        return this._x;
    }

    get y() {
        return this._y;
    }


    set y(y_) {
        this._y = y_;
    }

    set x(x_) {
        this._x = x_;
    }


}

export class Color {
    constructor(r_ = 255, g_ = 255, b_ = 255, a_ = 255) {
        this._r = r_;
        this._g = g_;
        this._b = b_;
        this._a = a_;

        this._setHEX();
    }

    _toHex(int_) {
        let hex = int_.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }

    _setHEX() {
        this._hex = "#" + this._toHex(this._r) + this._toHex(this._g) + this._toHex(this._b)
    }

    get HEX() {
        return this._hex;
    }

    static get Random() {
        return new Color(Math.randomInt(0, 255), Math.randomInt(0, 255), Math.randomInt(0, 255));
    }
}

//todo global but inly color need it
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
Math.randomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
};
// if(!!exports) {
//     exports.Size = Size;
//
// }


export class Rect {
    _points = []

    constructor(x_ = 0, y_ = 0, w_ = 0, h_ = 0/*, color_ = new Color(0,255,0), line_width_ = 4*/) {
        this._x = x_;
        this._y = y_;
        this._w = w_;
        this._h = h_;

        this.pointsClone;

    }

    static copy(rect_ = new Rect()) {
        return rect_.copy(rect_);
    }

    static Rect(x_ = 0, y_ = 0, w_ = 0, h_ = 0) {
        return new Rect(x_, y_, w_, h_);
    }

    copy(rect_ = new Rect(this._x, this._y, this._w, this._h)) {
        return new Rect(rect_._x, rect_._y, rect_._w, rect_._h)
    }

    set(rect_ = new Rect(this._x, this._y, this._w, this._h)) {
        this._x = rect_.x;
        this._y = rect_.y;
        this._w = rect_.width;
        this._h = rect_.height;
    }

    get width() {
        return this._w;
    }

    get height() {
        return this._h;
    }

    get x() {
        return this._x;
    }

    get y() {
        return this._y;
    }

    set width(width_) {
        this._w = width_;
    }

    set height(height_) {
        this._h = height_;
    }

    set y(y_) {
        this._y = y_;
    }

    set x(x_) {
        this._x = x_;
    }

    get points() {
        return this._points;
    }

    get pointsClone() {
        this._points[0] =  new Float32Array([this._x, this._y, 0, 1]);
        this._points[1] =  new Float32Array([this._x, this._y + this._h, 0, 1]);
        this._points[2] =  new Float32Array([this._x + this._w, this._y, 0, 1]);
        this._points[3] =  new Float32Array([this._x + this._w, this._y + this._h, 0, 1]);

        return this._points

    }
}