import { FpsMeter } from './utils/fpsMeter.js';
import anim from './animation/animator.js';
import TextureManager from './textures/textureManager.js';
import ShaderManager from './shaders/shaderManager.js';
import { RenderTarget } from './renderTarget.js';

export let core = null;
export let gl = null;
export let animator = null;
export let fpsMeter = null;
export const textureManager = TextureManager;
export const shaderManager = ShaderManager;
export let scene = null;

export class Core
{
    _width = 1280;
    _height = 700;
    _scene = null;
    _renderTarget = {};

    constructor(parentDiv = window.document.body)
    {
        this.initGL(parentDiv);
        this.initFPSMeter(parentDiv);
        this.initAnimator();
        this.initRenderTarget();
        core = this;

        window.onfocus = () =>
        {
            FpsMeter.lastTime = Date.now();
        };

        window.requestAnimationFrame(this.draw.bind(this));
    }
    initRenderTarget()
    {
        this._renderTarget = { main: new RenderTarget() };
        this._renderTarget.main.setCameraEventToDiv(this.canvas);
        this._scene = this._renderTarget.main._scene;
        scene = this._scene;
    }

    initAnimator()
    {
        this.animator = anim;
        animator = this.animator;
    }

    initGL(parentDiv)
    {
        this.canvas = document.createElement('canvas');
        this.canvas.style.width = `${this._width}px`;
        this.canvas.style.height = `${this._height}px`;
        this.canvas.width = this._width;
        this.canvas.height = this._height;

        parentDiv.append(this.canvas);
        // Initialize the GL context
        this.gl = this.canvas.getContext('webgl');
        this.gl.viewportWidth = this.gl.canvas.clientWidth;
        this.gl.viewportHeight = this.gl.canvas.clientHeight;

        gl = this.gl;
    }

    initFPSMeter(parentDiv)
    {
        this.fpsMeter = new FpsMeter(parentDiv);
        fpsMeter = this.fpsMeter;
    }
    get memoryUsedString()
    {
        return `${window.performance.memory.usedJSHeapSize * MB}/${window.performance.memory.totalJSHeapSize * MB}`;
    }

    draw()
    {
        this._renderTarget.main.draw();
        core.fpsMeter.draw(this.memoryUsedString);
        animator.animation(fpsMeter.delta);
        window.requestAnimationFrame(this.draw.bind(this));
    }
}

const MB = 1 / 1024 / 1024;

