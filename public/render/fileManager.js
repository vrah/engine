class FileManager
{
    // private:
    // leaded files
    _files = {};

    // for count loading;)
    _filesCount = 0;
    _filesLoading = 0;

    // public
    /**
     * Constructor
     */
    constructor()
    {
        this.init();
    }

    /**
     * empty now
     */
    init()
    {
    }

    /**
     * better out (not need more performance need better read code)
     * @param arrayFiles
     * @returns {[]}
     */
    getArrayFuntion(arrayFiles)
    {
        const arrayPromises = [];

        for (let i = 0; i < arrayFiles.length; i++)
        {
            this._filesCount++;
            arrayPromises.push(this.loadFile(arrayFiles[i]));
        }

        return arrayPromises;
    }

    /**
     * load async  files
     * @param arrayFiles {[]}
     * @returns {Promise<void>}
     */
    async loadFiles(arrayFiles)
    {
        const arrayPromises = this.getArrayFuntion(arrayFiles);

        const files = await Promise.all(arrayPromises);

        for (const id in files)
        {
            const fileName = files[id].fileName;

            this._files[fileName] = files[id].data;
        }
    }

    /**
     * get file by path
     * @param path{string} path to file
     * @returns {*}
     */
    getFile(path)
    {
        return this._files[path];
    }

    loadFileCounter(fileName)
    {
        this._filesLoading++;
        console.log(`load( file: ${fileName} ${this._filesLoading / this._filesCount}% ${this._filesLoading}/${this._filesCount})`);
    }

    async loadFile(fileName)
    {
        const type = FileManager.getTypeFile(fileName);
        const response = await fetch(fileName);

        if (response.status === 200)
        {
            const data = await FileManager.MAPPING_LOAD[type](response);

            this.loadFileCounter(fileName);

            return { fileName, data };
        }
    }

    /**
     * static mapping function by load by type
     * @static
     * hmm dont need static, but is better mapping for
     * dont need instance
     *
     * */
    static get MAPPING_LOAD()
    {
        return {
            undefined: async (response) =>
                null,
            bin: async (response) =>
                await FileManager.MAPPING_LOAD.undefined(arguments),

            gltf: async (response) =>
                await FileManager.MAPPING_LOAD.undefined(arguments),

            png: async (response) =>
            {
                const data = await response.blob();
                const url = URL.createObjectURL(data);

                return new Promise((resolve, reject) =>
                {
                    const img = new Image();

                    img.addEventListener('load', (e) =>
                    {
                        resolve(img);
                    });
                    img.addEventListener('error', () =>
                    {
                        reject(new Error(`Failed to load image's URL: ${url}`));
                    });
                    img.src = url;
                });
            },

            jpg: async (response) =>
                await FileManager.MAPPING_LOAD.png(response),

            json: (response) =>
                response.json(),
            glsl: (response) =>
                response.text(),
            zip: (response) =>
            {
                response.blob().then((data) =>
                {
                    const link = document.createElement('a');

                    link.href = URL.createObjectURL(data);
                    link.download = 'download';
                    link.click();

                    return link;
                },
                );
            },
        };
    }

    /**
     * static dunctio
     * static because need for other call without use instance
     * @static
     * @param filename
     * @returns {any}
     */
    static getTypeFile(filename)
    {
        const r = (/.+\.(.+)$/).exec(filename);

        return r ? r[1] : null;
    }
}

// global file manager
export default new FileManager();
