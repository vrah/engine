import { ShaderLocation } from './shaderLocation.js';

export class ShaderLocationArray extends ShaderLocation
{
    isArray = true;
    arrayNameIndex;
    arrayMax;
    pattern;
    constructor(key, src)
    {
        super(key);
        const tmp = key.split(' ');

        this.pattern = tmp[1];
        tmp[1] = tmp[1].split('[')[0];
        this.arrayNameIndex = key.split('[')[1].split(']')[0];
        this.arrayMax = parseInt(src.split(`${this.arrayNameIndex} `)[1].split('\n')[0]);

        this.type = tmp[0];
        this.name = tmp[1];
    }

    static isArray(key)
    {
        return key.includes('[');
    }
}
