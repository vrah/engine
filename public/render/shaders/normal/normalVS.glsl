//#version 120

#define MAX_MATRICES 1
attribute vec4 aVertexPosition;
attribute vec2 aTextureCoord;
attribute float aIndicateMatrix;
attribute float aColor;

uniform mat4 uMatrices[MAX_MATRICES];
uniform mat4 uModelMatrix;
uniform mat4 uViewMatrix;
uniform mat4 uProjectionMatrix;

uniform mat4 uProjectionViewMatrix;
varying highp vec2 vTextureCoord;
void main(void) {
//    gl_Position = aVertexPosition;
//    aMatrix;
    gl_Position = /*uProjectionViewMatrix **/ /*uModelMatrix **/ uMatrices[int(aIndicateMatrix)] * aVertexPosition;
   //s gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * aVertexPosition;

    vTextureCoord = aTextureCoord;
    //vTextureCoord = vec2(aVertexPosition.x, aVertexPosition.y);
}