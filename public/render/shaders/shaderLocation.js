export class ShaderLocation
{
    key;
    type;
    name;
    param = null;
    constructor(key)
    {
        this.key = key;
        const tmp = key.split(' ');

        this.type = tmp[0];
        this.name = tmp[1];
    }
}
