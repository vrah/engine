import { ShaderLocation } from './shaderLocation.js';
import { ShaderLocationArray } from './shaderLocationArray.js';

export default class ShaderProgram
{
    location = {
        attribute: [],
        uniform: [],
    };
    locationList = {};
    program = null;
    _gl = null;

    constructor(gl, vsSource, fsSource)
    {
        const vertexShader = this.loadShader(gl, gl.VERTEX_SHADER, vsSource);
        const fragmentShader = this.loadShader(gl, gl.FRAGMENT_SHADER, fsSource);

        this._gl = gl;

        const shaderProgram = gl.createProgram();

        gl.attachShader(shaderProgram, vertexShader);
        gl.attachShader(shaderProgram, fragmentShader);
        gl.linkProgram(shaderProgram);

        // If creating the shader program failed, alert

        if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS))
        {
            console.error(`Unable to initialize the shader program: ${gl.getProgramInfoLog(shaderProgram)}`);

            return;
        }

        this.program = shaderProgram;

        Object.keys(this.location).forEach((type) =>
        {
            this.initParam(vsSource, type);
            this.initParam(fsSource, type);
        });
        this.setLocation();
        this.setListLocation();
    }

    loadShader(gl, type, source)
    {
        const shader = gl.createShader(type);

        // Send the source to the shader object
        gl.shaderSource(shader, source);

        // Compile the shader program
        gl.compileShader(shader);

        // See if it compiled successfully
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
        {
            console.error(`An error occurred compiling the shaders: ${gl.getShaderInfoLog(shader)}`);

            gl.deleteShader(shader);

            return null;
        }

        return shader;
    }

    setListLocation()
    {
        this.location.attribute.forEach((item, key) =>
        {
            this.locationList[item.name] = item;
        });

        this.location.uniform.forEach((item, key) =>
        {
            this.locationList[item.name] = item;
        });
    }

    setLocation()
    {
        this.location.attribute.forEach((item, key) =>
        {
            item.param = this._gl.getAttribLocation(this.program, item.name);
            if (item.param < 0)
            {
                console.error('wtf getAttribLocation return -1 item name:', item.name);
                delete this.location.attribute[key];
                this.location.attribute.splice(key, 1);

                return;
            }
            this._gl.enableVertexAttribArray(item.param);
        });

        this.location.uniform.forEach((item) =>
        {
            if (item.isArray)
            {
                this.setLocationUniformArray(item);
            }
            else
            {
                item.param = this._gl.getUniformLocation(this.program, item.name);
            }
        });
    }

    setLocationUniformArray(item)
    {
        item.param = [];
        for (let i = 0; i < item.arrayMax; i++)
        {
            let pattern = item.pattern;

            pattern = pattern.replace(item.arrayNameIndex, i);
            item.param.push(this._gl.getUniformLocation(this.program, pattern));
        }
    }

    initParam(src, type)
    {
        const value = src.split(';').map((key) =>
        {
            const val = key.split(type)[1];

            if (val)
            { return val.trim(); }
        }).filter((key) => !!key,
        ).map((key) =>
        {
            if (ShaderLocationArray.isArray(key))
            {
                return new ShaderLocationArray(key, src);
            }

            return new ShaderLocation(key, src);
        });

        this.location[type] = this.location[type].concat(value);
    }
}

