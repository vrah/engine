import Shader from './shader.js';
import FileManager from '../fileManager.js';
import { gl } from '../core.js';

class ShaderManager
{
    _shaders = {};

    constructor()
    {
    }

    getShader(nameVertexShader, nameFragmentShader)
    {
        const id = nameVertexShader + nameFragmentShader;

        if (this._shaders[id])
        {
            return this._shaders[id];
        }
        console.log('init shader id: ', id);

        return this._shaders[id] = new Shader(gl,
            FileManager.getFile(nameVertexShader),
            FileManager.getFile(nameFragmentShader));
    }
}

export default new ShaderManager();
