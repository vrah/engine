//#version 120
//precision mediump float;
#define MAX_MATRICES 1000
attribute vec4 aVertexPosition;
attribute vec2 aTextureCoord;
attribute float aIndicateMatrix;
attribute float aColor;

uniform mat4 uMatrices[MAX_MATRICES];
uniform mat4 uModelMatrix;
uniform mat4 uViewMatrix;
uniform mat4 uProjectionMatrix;

uniform mat4 uProjectionViewMatrix;
varying highp vec2 vTextureCoord;

void main(void) {
    gl_Position = uMatrices[int(aIndicateMatrix)] *aVertexPosition;
    vTextureCoord = aTextureCoord;
}