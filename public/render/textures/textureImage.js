import { Rect, Size } from '../objects/primitive.js';
import { Texture } from './texture.js';

export class TextureImage extends Texture
{
    constructor(image, gl, config = Texture.CONFIG_STANDART.STANDART)
    {
        super();
        this.initTexture(image, gl, config);
    }
    initTexture(image, gl, config)
    {
        this._config = config;
        this._image = image;
        this._size = new Size(image.width, image.height);
        this._rect = new Rect(0, 0, image.width, image.height);
        this._rectUV = new Rect(0, 0, 1, 1);

        if (config & Texture.CONFIG.TEXTURE_2D)
        {
            this.set2DTexture(image, gl, config);
            this.set2DTextureWrap(image, gl, config);
        }
    }
}
