import { Texture } from './texture.js';
import { TextureImage } from './textureImage.js';
import { SpriteSheet } from './spriteSheet.js ';
import { gl } from '../core.js';
import FileManager from '../fileManager.js';
import { Rect } from '../objects/primitive.js';

export class TextureManager
{
    _textures = {};
    // eslint-disable-next-line no-useless-constructor
    constructor()
    {
    }

    get textures()
    {
        return this._textures;
    }

    getTexture(name)
    {
        const spriteSheet = this._textures[name];

        if (spriteSheet)
        {
            return spriteSheet;
        }
        const image = FileManager.getFile(name);

        if (!image)
        {
            // eslint-disable-next-line no-console
            console.log(`error`);
        }
        const texture = new TextureImage(image, gl);

        texture.name = name;

        return this._textures[name] = texture;

        // eslint-disable-next-line no-unreachable
        console.log(`error`);
    }

    addSpriteSheet(textureName, jsonName)
    {
        const texture = this.getTexture(textureName, TextureManager.TEXTURE);
        const json = FileManager.getFile(jsonName);
        const frames = json.frames;

        for (let i = 0; i < frames.length; i++)
        {
            const item = frames[i];
            const filename = item.filename;
            const frame = frames[i].frame;
            const rect = new Rect(frame.x, frame.y, frame.w, frame.h);

            const spriteSheet = new SpriteSheet(texture, rect);

            this._textures[filename] = spriteSheet;
        }
    }
}

export default new TextureManager();
