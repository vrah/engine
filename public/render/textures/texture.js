
import { Rect } from './../objects/primitive.js';

export class Texture
{
    _image;// = null;//new Image();
    _size;// = new Size();
    _texture;
    _name;

    _rect= new Rect(0, 0, 1, 1);
    // will be in sprite sheet
    _rectUV = new Rect(0, 0, 1, 1);
    _config;

    constructor(props)
    {
    }

    get config()
    {
        return this._config;
    }

    get flipY()
    {
        return this._config & Texture.CONFIG.UNPACK_FLIP_Y;
    }

    get rectUV()
    {
        return this._rectUV;
    }

    get rect()
    {
        return this._rect;
    }

    get size()
    {
        return this._size;
    }

    set name(name)
    {
        this._name = name;
    }

    get name()
    {
        return this._name;
    }
    get texture()
    {
        return this._texture;
    }

    set2DTexture(image, gl, config)
    {
        this._texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, this._texture);

        if (config & Texture.CONFIG.UNPACK_FLIP_Y)
        {
            gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        }

        if (config & Texture.CONFIG.TEXTURE_SRC_RGBA)
        {
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA,
                gl.RGBA, gl.UNSIGNED_BYTE, image);
        }
    }

    set2DTextureWrap(image, gl, config)
    {
        if (Texture.isPowerOf2(image.width) && Texture.isPowerOf2(image.height))
        {
            // Yes, it's a power of 2. Generate mips.
            gl.generateMipmap(gl.TEXTURE_2D);
        }

        if (config & Texture.CONFIG.TEXTURE_WRAP_S_CLAMP_TO_EDGE)
        {
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        }
        else if (config & Texture.CONFIG.TEXTURE_WRAP_S_REPEAT)
        {
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
        }

        if (config & Texture.CONFIG.TEXTURE_WRAP_T_CLAMP_TO_EDGE)
        {
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        }
        else if (config & Texture.CONFIG.TEXTURE_WRAP_T_REPEAT)
        {
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
        }

        if (config & Texture.CONFIG.TEXTURE_MIN_FILTER_LINEAR)
        {
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        }
    }

    /**
     * not need only here = static for me
     * @param value
     * @returns {boolean}
     */
    static isPowerOf2(value)
    {
        return (value & (value - 1)) === 0;
    }

    // =max 53
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/MAX_SAFE_INTEGER
    static get CONFIG()
    {
        return {
            TEXTURE_2D: 0b1,
            TEXTURE_MAG_FILTER_LINEAR: 0b10,
            TEXTURE_MAG_FILTER_NEAREST: 0b100,
            TEXTURE_MIN_FILTER_LINEAR: 0b1000,
            TEXTURE_MIN_FILTER_NEAREST: 0b10000,
            TEXTURE_WRAP_S_REPEAT: 0b100000,
            TEXTURE_WRAP_S_CLAMP_TO_EDGE: 0b1000000,
            TEXTURE_WRAP_T_REPEAT: 0b10000000,
            TEXTURE_WRAP_T_CLAMP_TO_EDGE: 0b100000000,
            TEXTURE_MIN_FILTER_NEAREST_MIPMAP_NEAREST: 0b1000000000,
            TEXTURE_MIN_FILTER_LINEAR_MIPMAP_NEAREST: 0b10000000000,
            TEXTURE_MIN_FILTER_NEAREST_MIPMAP_LINEAR: 0b100000000000,
            TEXTURE_MIN_FILTER_LINEAR_MIPMAP_LINEAR: 0b1000000000000,
            TEXTURE_SRC_RGBA: 0b10000000000000,
            TEXTURE_SRC_RGB: 0b100000000000000,
            UNPACK_FLIP_Y:  0b1000000000000000,

        };
    }

    static get CONFIG_STANDART()
    {
        return {
            STANDART: Texture.CONFIG.TEXTURE_2D
                | Texture.CONFIG.TEXTURE_MAG_FILTER_LINEAR
                | Texture.CONFIG.TEXTURE_MIN_FILTER_LINEAR
                | Texture.CONFIG.TEXTURE_WRAP_S_CLAMP_TO_EDGE
                | Texture.CONFIG.TEXTURE_WRAP_T_CLAMP_TO_EDGE
                | Texture.CONFIG.TEXTURE_SRC_RGBA
                | Texture.CONFIG.UNPACK_FLIP_Y,

        };
    }

    // static CONFIG = {
    //     TEXTURE_2D                      : 0b1,
    //     TEXTURE_MAG_FILTER_LINEAR       : 0b10,
    //     TEXTURE_MAG_FILTER_NEAREST      : 0b100,
    //     TEXTURE_MIN_FILTER_LINEAR       : 0b1000,
    //     TEXTURE_MIN_FILTER_NEAREST      : 0b10000,
    //     TEXTURE_WRAP_S_REPEAT           : 0b100000,
    //     TEXTURE_WRAP_S_CLAMP_TO_EDGE    : 0b1000000,
    //     TEXTURE_WRAP_T_REPEAT           : 0b10000000,
    //     TEXTURE_WRAP_T_CLAMP_TO_EDGE    : 0b100000000,
    //     TEXTURE_MIN_FILTER_NEAREST_MIPMAP_NEAREST   : 0b1000000000,
    //     TEXTURE_MIN_FILTER_LINEAR_MIPMAP_NEAREST    : 0b10000000000,
    //     TEXTURE_MIN_FILTER_NEAREST_MIPMAP_LINEAR    : 0b100000000000,
    //     TEXTURE_MIN_FILTER_LINEAR_MIPMAP_LINEAR     : 0b1000000000000,
    //     TEXTURE_SRC_RGBA     : 0b10000000000000,
    //     TEXTURE_SRC_RGB     : 0b100000000000000,
    //
    // }
    //
    // static CONFIG_STANDART = {
    //     STANDART: Texture.CONFIG.TEXTURE_2D | Texture.CONFIG.TEXTURE_MAG_FILTER_LINEAR | Texture.CONFIG.TEXTURE_MIN_FILTER_LINEAR |
    //         Texture.CONFIG.TEXTURE_WRAP_S_CLAMP_TO_EDGE | Texture.CONFIG.TEXTURE_WRAP_T_CLAMP_TO_EDGE |  Texture.CONFIG.TEXTURE_SRC_RGBA
    // }
}

