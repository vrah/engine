import { Texture } from './texture.js';
import { Rect, Size } from '../objects/primitive.js';

export class SpriteSheet extends Texture
{
    constructor(texture, rect)
    {
        super();
        this.initTexture(texture, rect);
    }

    setRectUVFlipY(texture, rect)
    {
        this._rectUV = new Rect(
            ((rect.x) / texture._size.width),
            ((texture._size.height - rect.height - rect.y) / texture._size.height),
            (rect.width) / texture._size.width,
            (rect.height) / texture._size.height);
    }

    setRectUV(texture, rect)
    {
        this._rectUV = new Rect(
            rect.x / texture._size.width,
            rect.y / texture._size.height,
            (rect.width) / texture._size.width,
            (rect.height) / texture._size.height);
    }

    initTexture(texture, rect)
    {
        this._texture = texture.texture;
        this._name = texture._name;
        this._image = texture._image;
        this._size = new Size(rect.width, rect.height);
        this._rect = new Rect(0, 0, rect.width, rect.height);
        this._config = texture.config;

        if (this.flipY)
        {
            this.setRectUVFlipY(texture, rect);
        }
        else
        {
            this.setRectUV(texture, rect);
        }
    }
}
