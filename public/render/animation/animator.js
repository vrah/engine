// /**
//  * Created by jankr on 29.01.2017.
//  */
// "use strict"
export class Animator
{
    constructor()
    {
        this._animations = {};
    }
    animation(dt)
    {
        for (const i in this._animations)
        {
            this._animations[i].animation(dt);
            // const tmp_animation = this._animations[i].animation(dt);
            // tmp_animation.animation(dt);
        }
    }

    addAnimation(animation)
    {
        const id = animation.id;
        this._animations[id] = animation;
    }

    clearAnimationById(id)
    {
        delete this._animations[id];
    }

    clearAnimation(animation)
    {
        const id = animation.id;
        delete this._animations[id];
    }

    /**
     * more slowe i thnk
     * @param node_
     */
    clearAnimationByNode(node)
    {
        for (const i in this._animations)
        {
            const animation = this._animations[i];

            if (animation.node.id === node.id)
            {
                console.log('clear node', node.id);
                delete this._animations[animation.id];
            }
        }
    }
}

export default new Animator();

// if(!!exports) {
//     // this = new Animator()
//     module.exports = Animator;
// }
