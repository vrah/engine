// /**
//  * Created by jankr on 29.01.2017.
//  */
// "use strict"

import { vec2 } from '../third_party/glMatrix/index.js';
import { animator } from '../core.js';

let g_count_animation = 0;

export class Animation
{
    _animationTime = 0;
    _timeCurrent = 0;
    _duration = 0;
    // todo hash
    _id = g_count_animation++;

    constructor()
    {

    }

    clear()
    {
        animator.clearAnimationById(this._id);
    }

    set node(node)
    {
        this._node = node;
    }

    get node()
    {
        return this._node;
    }

    addNode(node = null)
    {
        this._node = node;
    }

    get id()
    {
        return this._id;
    }

    set id(id)
    {
        this._id = id;
    }

    /**
     * aniamtion function
     * @param dt {number} delta
     */
    animation(dt)
    {
        this._animationTime += dt;
        this._timeCurrent = this._animationTime / this._duration;
    }

    checkEndAndCall()
    {
        if (this._timeCurrent > 1)
        {
            this.clear();
        }
    }
}

export class MoveTo extends Animation
{
    /**
     * @constructor
     * @param duration {number}
     * @param position {vec2}
     */
    constructor(duration = 1.0, position = vec2.create())
    {
        super();
        this._positionEnd = position;
        this._positionStart = position;
        this._duration = duration * 1000;
    }

    animation(dt)
    {
        super.animation(dt);

        const percent = dt / this._duration;
        const tmpNode = this._node;
        const tmpPositionNow = vec2.create();

        vec2.scale(tmpPositionNow, this._vec, percent);
        tmpNode.position = vec2.add(tmpNode.position, tmpNode.position, tmpPositionNow);

        this.checkEndAndCall();
    }

    initStartPosition()
    {
        this._positionStart = this._node.position;
        this._vec = vec2.create();
        vec2.subtract(this._vec, this._positionEnd, this._positionStart);
    }

    get node()
    {
        return super.node;
    }

    set node(node)
    {
        super.node = node;
        this.initStartPosition();
    }
}

export class MoveBy extends Animation
{
    constructor(duration, position = vec2.create())
    {
        super();
        this._vec = position;
        this._duration = duration * 1000;
    }

    animation(dt)
    {
        super.animation(dt);

        const percent = dt / this._duration;
        const tmpNode = this._node;
        const tmpPositionNow = vec2.create();

        vec2.scale(tmpPositionNow, this._vec, percent);

        tmpNode.position = vec2.add(tmpNode.position, tmpNode.position, tmpPositionNow);

        this.checkEndAndCall();
    }
}

export class Scale2DBy extends Animation
{
    constructor(duration, scale = vec2.create())
    {
        super();
        this._vec = scale;

        this._duration = duration * 1000;
    }

    animation(dt)
    {
        super.animation(dt);

        const percent = dt / this._duration;
        const tmpNode = this._node;
        const tmpScaleNow = vec2.create();

        vec2.scale(tmpScaleNow, this._vec, percent);
        // console.log(/*tmpScaleNow,this._vec,timeCurrent,*/ tmpNode.scale)
        vec2.add(tmpNode.scale, tmpNode.scale, tmpScaleNow);

        this.checkEndAndCall();
    }
}

export class Rotation2DBy extends Animation
{
    constructor(duration, rotation = vec2.create())
    {
        super();
        this._vec = rotation;

        this._duration = duration * 1000;
    }

    animation(dt)
    {
        super.animation(dt);

        const percent = dt / this._duration;
        const tmpNode = this._node;
        const tmpPositionNow = vec2.create();

        vec2.scale(tmpPositionNow, this._vec, percent);

        tmpNode.position = vec2.add(tmpNode.position, tmpNode.position, tmpPositionNow);

        this.checkEndAndCall();
    }
}

export class SpriteSheetAnimation extends Animation
{
    constructor(durationPerFrame = 0.1, spriteArray = [], loop = true)
    {
        super();
        // 1 / 100
        this.durationPerFrame = 1 / (durationPerFrame * 1000);
        this._spriteArray = spriteArray;
        this.countSprites = this._spriteArray.length;
        this._loop = loop;
        this._lastFrameIndex = -1;
        this._currentFrame = '';
    }

    animation(dt)
    {
        this._animationTime += dt;
        this._timeCurrent = this._animationTime / this._duration;
        const time = this._animationTime * this.durationPerFrame;
        let frame_index = Math.floor(time);

        if (!this._loop && frame_index === this.countSprites)
        {
            console.log('end');
            this.clear();
        }

        frame_index %= this.countSprites;
        // bad better is _currentFrame to next frame
        if (this._lastFrameIndex === frame_index)
        {
            return;
        }
        this._lastFrameIndex = frame_index;
        this._currentFrame = this._spriteArray[frame_index];

        // this.node.texture = this._currentFrame;
        this.node.textureName = this._currentFrame;
    }
}

export class Sequence extends Animation
{
    constructor(arrayAnimation = [])
    {
        super();

        this._animationArray = arrayAnimation;
    }

    animation(dt)
    {
        const anim = this._animationArray[0];

        anim.clear = () =>
        {
            const anim = this._animationArray.shift();

            animator.clearAnimationById(anim.id);
            const newAnim = this._animationArray[0];

            if (!newAnim)
            {
                this.clear();

                return;
            }

            newAnim.node = this.node;
        };

        anim.animation(dt);
    }

    set node(node)
    {
        this._animationArray[0].node = node;
        this._node = node;
    }

    get node()
    {
        return this._node;
    }
}

export class CallFunction extends Animation
{
    constructor(cb)
    {
        super();
        this._cb = cb;
    }

    animation()
    {
        this.clear();
        this._cb();
    }
}

export class DelayTime extends Animation
{
    constructor(duration, position = vec2.create())
    {
        super();
        this._positionEnd = position;
        this._positionStart = position;
        this._duration = duration * 1000;
    }

    animation(dt)
    {
        super.animation(dt);
        this.checkEndAndCall();
    }
}
