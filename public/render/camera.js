import { mat4, vec3, vec2 } from './third_party/glMatrix/index.js';
import {gl} from './core.js'
const VECTOR_ROTATION_PITCH = vec3.clone([-1, 0, 0]);
const VECTOR_ROTATION_YAW = vec3.clone([0, -1, 0]);

export class Camera
{
    uProjectionMatrix = mat4.create();
    uViewMatrix = mat4.create();
    uPointCamera = vec3.create();
    uProjectionViewMatrix = mat4.create();

    position = vec3.clone([0, 0, -660]);
    lastMouse = [0, 0];
    pitch = 0;
    yaw = 0;
    scroll= 1;
    positionMouseOnScreen = vec2.create();
    static get MOUSE_LEFT_DOWN() { return 1; }
    static get MOUSE_RIGHT_DOWN() { return 2; }
    static get MOUSE_MIDDLE_DOWN() { return 4; }
    static get VECTOR_ROTATION_PITCH() { return VECTOR_ROTATION_PITCH; }
    static get VECTOR_ROTATION_YAW() { return VECTOR_ROTATION_YAW; }
    static get DEG_TO_RAD() { return 0.0174532925; }
    static get RAD_TO_DEG() { return 57.29577951308; }
    static get PRECISION_ROTATION() { return 1; }
    static get PRECISION_MOVE() { return 1; }

    // static MOUSE_LEFT_DOWN = 1;
    // static MOUSE_RIGHT_DOWN = 2;
    // static MOUSE_MIDDLE_DOWN = 4;
    // static VECTOR_ROTATION_PITCH = vec3.clone([-1, 0, 0]);
    // static VECTOR_ROTATION_YAW = vec3.clone([0, -1, 0])
    // static DEG_TO_RAD = 0.0174532925; // 2*Math.PI/360
    // static RAD_TO_DEG = 57.29577951308; //360/(2*Math.PI)
    // static PRECISION_ROTATION = 0.6;
    // static PRECISION_MOVE = 0.01;

    constructor()
    {
        this._gl = gl;
        this.updatePerspectiveMatrix();
        this.updateViewMatrix();
    }

    setModelViewMatrix(shader)
    {
        gl.uniformMatrix4fv(
            shader.locationList.uModelViewMatrix.param,
            false,
            this.uViewMatrix);
    }

    setViewMatrix(shader)
    {
        gl.uniformMatrix4fv(
            shader.locationList.uViewMatrix.param,
            false,
            this.uViewMatrix);
    }

    setProjectionMatrix( shader)
    {
        gl.uniformMatrix4fv(
            shader.locationList.uProjectionMatrix.param,
            false,
            this.uProjectionMatrix);
    }

    setProjectionViewMatrix(gl, shader)
    {
        gl.uniformMatrix4fv(
            shader.locationList.uProjectionViewMatrix.param,
            false,
            this.uProjectionViewMatrix);
    }

    get modelViewMatrix()
    {
        return this.uViewMatrix;
    }

    get projectionMatrix()
    {
        return this.uProjectionMatrix;
    }

    updatePerspectiveMatrix()
    {

        // const fieldOfView = 45 * Math.PI / 180;   // in radians
        // const aspect = this._gl.viewportWidth / this._gl.viewportHeight;
        // const zNear = 0.1;
        // const zFar = 10000000.0;
        // this.uProjectionMatrix = mat4.create();
        //
        // // note: glmatrix.js always has the first argument
        // // as the destination to receive the result.
        // mat4.perspective(this.uProjectionMatrix,
        //     fieldOfView,
        //     aspect,
        //     zNear,
        //     zFar);

        mat4.perspective(this.uProjectionMatrix, 45, this._gl.viewportWidth / this._gl.viewportHeight, 0.1, 100000.0);
    }

    updateViewMatrix()
    {
        mat4.identity(this.uViewMatrix);
        mat4.translate(this.uViewMatrix, this.uViewMatrix, this.position);
        mat4.rotate(this.uViewMatrix, this.uViewMatrix, Camera.DEG_TO_RAD * this.pitch, Camera.VECTOR_ROTATION_PITCH);
        mat4.rotate(this.uViewMatrix, this.uViewMatrix, Camera.DEG_TO_RAD * this.yaw, Camera.VECTOR_ROTATION_YAW);

        mat4.multiply(this.uProjectionViewMatrix, this.uProjectionMatrix, this.uViewMatrix);
    }

    getMouseDifference(event)
    {
        const newX = event.movementX; /* ||
            event.mozMovementX ||
            /!*event.webkitMovementX ||*!/ event.clientX - this.lastMouse[0] ||
            0,*/
        const newY = event.movementY;/* ||
                event.mozMovementY ||
                /!* event.webkitMovementY ||*!/ event.clientY - this.lastMouse[1] ||
                0;*/
        this.lastMouse = [event.clientX, event.clientY];

        return [newX, newY];
    }

    rotation(movementX, movementY)
    {
        this.yaw -= movementX * Camera.PRECISION_ROTATION || 0;
        this.pitch -= movementY * Camera.PRECISION_ROTATION || 0;
    }

    move(movementX, movementY)
    {
        this.position[0] += movementX * Camera.PRECISION_MOVE;
        this.position[1] -= movementY * Camera.PRECISION_MOVE;
    }

    setPositionMouse(event) {

        this.positionMouseOnScreen[0] = ( event.clientX / (gl.viewportWidth * 0.5)) - 1.0;
        this.positionMouseOnScreen[1] = 1.0 - event.clientY / (gl.viewportHeight * 0.5);

        console.log(`x:${this.positionMouseOnScreen[0].toPrecision(3)},y: ${this.positionMouseOnScreen[1].toPrecision(3)}`)
    }

    get ray() {
        //positionMouseOnScreen
    }

    handleMouseMove(event)
    {
        // console.log(this.getMouseDifference(event))
        if (event.buttons & Camera.MOUSE_LEFT_DOWN)
        {
            this.rotation.apply(this, this.getMouseDifference(event));
        }

        if (event.buttons & Camera.MOUSE_MIDDLE_DOWN)
        {
            this.move.apply(this, this.getMouseDifference(event));
        }
        this.updateViewMatrix();
        this.setPositionMouse(event);
    }

    // try change enevent
    handleMouseWheel(event)
    {
        this.position[2] += (event.wheelDelta / this.scroll) || -event.deltaY;
        this.updateViewMatrix();
    }

    // try change enevent
    handleMouseDown(event)
    {
        event.preventDefault();
        event.stopPropagation();
    }

    // try change enevent
    handleMouseUp(event)
    {
        event.preventDefault();
        event.stopPropagation();
    }
}
