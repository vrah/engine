import { ArrayBuffer } from './arrayBuffer.js';
import { gl } from '../core.js';

export class ItemData
{
    get offset() {
        return this._offset;
    }

    set offset(value) {
        this._offset = value;
    }
    get type()
    {
        return this._type;
    }
    get param()
    {
        return this._param;
    }
    get sizeByte()
    {
        return this._sizeByte;
    }
    get offsetByte()
    {
        return this._offsetByte;
    }

    set offsetByte(value)
    {
        this._offsetByte = value;
    }
    get size()
    {
        return this._size;
    }
    get count()
    {
        return this._count;
    }

    get deep()
    {
        return this._deep;
    }
    _size = 0;
    _count = 0;
    _deep = 0;
    _data;
    _param;
    _type = gl.FLOAT;
    _sizeByte = 0;
    _offsetByte = 0;// compute whne load array Items
    _offset = 0;

    setConfig(size, count, deep, type)
    {
        this._size = size; // vec4 vec3 vec2 vec1 etc
        this._count = count; // count trinagle
        this._deep = deep;
        this._type = ArrayBuffer.TYPE[type]; // gl.FLoat
        this._sizeByte = this._size * window[type].BYTES_PER_ELEMENT;
    }

    getStruct(data, count, deep)
    {
        if (!data[0].length)
        {
            this.setConfig(data.length,
                count,
                deep,
                data.constructor.name);
        }
        else
        {
            deep++;
            this.getStruct(data[0], data.length, deep);
        }
    }

    constructor(data, param)
    {
        this._data = data;
        this._param = param;

        // todo will be recursion!!
        if (this._data && this._data.length)
        {
            this.getStruct(data, 0, 1);
        }
        else if (this._data !== undefined)
        {
            this.setConfig(1,
                1,
                0,
                ArrayBuffer.TYPE[this._data]);
        }
    }
    set data(data)
    {
        this._data = data;
    }
}
