import { gl } from '../core.js';
import { ArrayBuffer } from './arrayBuffer.js';

export class ArrayBufferFull extends ArrayBuffer
{
    _struct;
    _index;
    _stride = 0;
    _countTriangle = 4;
    _countObjects = 1;
    _offsetByVertex = 0
    constructor(data, countObjects = 1, target = gl.ARRAY_BUFFER, usage = gl.DYNAMIC_DRAW)
{
        super(target, usage);
        this._struct = data;
        this._countObjects = countObjects;
        let sizeByte = 0;

        for (const id in this._struct)
        {
            const item = this._struct[id];

            item.offsetByte = sizeByte;
            item.offset = this._offsetByVertex;
            this._offsetByVertex += item.size;
            sizeByte += item.sizeByte;
            this._countTriangle = item.count;
        }

        this._stride = sizeByte;
        this._data = new Float32Array(this._stride * this._countObjects);

    }

    set countObject(countObject) {
        this._countObjects = countObject;
        this._data = new Float32Array(this._stride * this._countObjects);
    }

    setBufferUpdate(data, indexObject = 0)
    {
        const offsetObject =  indexObject * this._countTriangle * this._offsetByVertex;


        let offsetVertex = 0;
        let indexInArray = 0;
        let vertex = 0;
        let valueIndex = 0;

        for (const id in data)
        {
            const itemData = data[id];
            const item = this._struct[id];
            vertex = 0;
            while (vertex < item.count)
            {
                offsetVertex = (vertex * this._offsetByVertex);
                valueIndex = 0;
                while (valueIndex < item.size)
                {
                    indexInArray = offsetObject + offsetVertex + item.offset + valueIndex;

                    this._data[indexInArray] = itemData[vertex][valueIndex];
                    valueIndex++;
                }
                vertex++;
            }
        }


        super.setBuffer(this._data);
    }

    drawBuffer()
    {
        gl.bindBuffer(this._target, this._buffer);
        for (const id in this._struct)
        {
            const item = this._struct[id];

            gl.vertexAttribPointer(item.param,
                item.size,
                item.type,
                false,
                this._stride,
                item.offsetByte);
            gl.enableVertexAttribArray(item.param);
        }
    }
}
