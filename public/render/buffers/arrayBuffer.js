import { gl } from '../core.js';

export class ArrayBuffer
{
    // https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/Types
    _type = gl.FLOAT;
    _offset = 0;
    _data = null;
    _buffer = gl.createBuffer();
    _count = 0;
    _sizeByte = 0;
    _target = gl.ARRAY_BUFFER;
    _usage = gl.DYNAMIC_DRAW;

    /**
     * @constructor
     * @param data {Float32Array | Uint16Array | etc} data
     * @param target {GLenum} gl.ARRAY_BUFFER | gl.ELEMENT_ARRAY_BUFFER
     * @param usage {GLenum} gl.DYNAMIC_DRAW | gl.STATIC_DRAW
     */
    constructor(target = this._target, usage = this._usage)
    {
        this._target = target;
        this._usage = usage;
    }

    setTypeByData(data)
    {
        if (data.constructor && data.constructor.name)
        {
            this._type = ArrayBuffer.TYPE[data.constructor.name];
        }
    }
    setSize(count = this._count, type = this._type)
    {
        this._sizeByte = count * window[this._data.constructor.name].BYTES_PER_ELEMENT;
    }
    setBufferUpdate(data)
    {
        this._data = data;
        this.setTypeByData(this._data);
        this._count = this._data.length;
        this.setSize();


        gl.bindBuffer(this._target, this._buffer);
        gl.bufferData(this._target,
            this._data,
            // new Float32Array(data),
            this._usage);

        this.setBuffer();
    }
    setBuffer()
    {
        gl.bindBuffer(this._target, this._buffer);
        gl.bufferData(this._target, this._data, this._usage);
    }

    get buffer()
    {
        return this._buffer;
    }

    static get TYPE()
    {
        return {
            Float32Array: gl.FLOAT,
            Uint16Array: gl.UNSIGNED_SHORT,
        };
    }

    static get ID_BY_TYPE()
    {
        return {
            'gl.FLOAT': gl.FLOAT,
            'gl.UNSIGNED_SHORT': gl.UNSIGNED_SHORT,
        };
    }
}
