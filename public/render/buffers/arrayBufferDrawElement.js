import { gl } from '../core.js';
import { ArrayBuffer } from './arrayBuffer.js';

export class ArrayBufferDrawElement extends ArrayBuffer
{
    _target = gl.ELEMENT_ARRAY_BUFFER
    _usage = gl.DYNAMIC_DRAW;
    _mode = gl.TRIANGLES;

    constructor(target = gl.ELEMENT_ARRAY_BUFFER, usage = gl.DYNAMIC_DRAW)
    {
        super(target, usage);
    }

    drawBuffer()
    {
        // https://developer.mozilla.org/en-US/docs/Web/API/WebGLRenderingContext/drawElements
        gl.bindBuffer(this._target, this._buffer);
        gl.drawElements(this._mode, this._count, this._type, this._offset);
    }
}
