import { Core, core, gl, fpsMeter, animator, textureManager, scene } from './../render/core.js';
import FileManager from './../render/fileManager.js';
import {
    MoveTo,
    MoveBy,
    DelayTime,
    CallFunction,
    Sequence,
    Scale2DBy,
    SpriteSheetAnimation,
} from './../render/animation/animation.js';
import { vec2 } from './../render/third_party/glMatrix/index.js';
import { Text } from './../render/objects/text.js';
import { Sprite } from './../render/objects/index.js';

const SCALE = 1;
const SCALE_ANIMATION = 0.5;
const ANIMATION = {
    sasuke: {
        chidory: {
            end: 168,
            start: 127,
            name: `chidori/sasuke_sheet_2_`,
            duration: 4,
        },
        fire: {
            end: 88,
            start: 71,
            name: `fire/fire`,
            duration: 1,
        },
    },

    megaMan: {
        // eslint-disable-next-line camelcase
        port_leave: {
            end: 101,
            start: 87,
            name: `port_leave/`,
            duration: 1,
        },
        run: {
            end: 44,
            start: 34,
            name: `run/`,
            duration: 1,
        },

    },
};

window.onload = async () =>
{
    await FileManager.loadFiles([
        './render/shaders/normal/normalFS.glsl',
        './render/shaders/normal/normalVS.glsl',
        './render/shaders/batch/normalFS.glsl',
        './render/shaders/batch/normalVS.glsl',
        './res/sasuke.png',
        './res/sasuke.json',
        './res/mega_man.png',
        './res/mega_man.json',
        './res/panda.jpg',
        // './res/space.bin',
        // './res/space.gltf',
    ]);
    // debugger
    // let s = new Scene();
    const test = await new Test('glCanvas');
};

class Test
{
    // sceneManager = new SceneManager();

    constructor()
    {
        const core = new Core();

        textureManager.addSpriteSheet(`./res/sasuke.png`, './res/sasuke.json');
        textureManager.addSpriteSheet(`./res/mega_man.png`, './res/mega_man.json');
        this.createFPSMetter();
        const NUM = 30;
        const pos = [-1, -1, 0];
        let indexDeep = 0;
        const indexDeepCount = 1;
        const nodesDeep = [scene];
        let parent;
        let indexAnimation = 0;
        // debugger;
        const animations = [
            ANIMATION.sasuke.chidory,
            ANIMATION.sasuke.fire,
            ANIMATION.megaMan.port_leave,
            ANIMATION.megaMan.run,
        ];
        //
        // const panda = new Sprite('./res/panda.jpg');
        // scene.addChild(panda)
        //
        // const fullTextS = new Sprite(`./res/sasuke.png`);
        // scene.addChild(fullTextS);

        // const fullTextM = new Sprite(`./res/mega_man.png`);
        // scene.addChild(fullTextM)


            // const sprite1 = new Sprite(`base/sasuke_sheet_2_03.png`);
            // const sprite2 = new Sprite(`fire/fire88.png`);
            //
            // scene.addChild(sprite1);
            // sprite1.addChild(sprite2);
            //
            // const a0 = new DelayTime(0.5 + Math.random() * 0.5);
            // sprite1.runAnimation(new Sequence([a0, this.createAnimSpriteSheet(ANIMATION.sasuke.chidory)]));
            // const a1 = new DelayTime(0.5 + Math.random() * 0.5);
            // sprite2.runAnimation(new Sequence([a1, this.createAnimSpriteSheet(ANIMATION.sasuke.fire)]));

        // return;
        for (let i = 0; i < NUM; i++)
        {
            for (let j = 0; j < NUM; j++)
            {
                // start()
                const sprite = new Sprite(`base/sasuke_sheet_2_03.png`);
                // let text = new Text(`deep: ${indexDeep}`)
                // text.position =[0,-100,0];
                // sprite.addChild(text)

                parent = nodesDeep[indexDeep];
                indexDeep = (indexDeep + 1) % indexDeepCount;

                sprite.scale = [SCALE, SCALE, SCALE];

                sprite.position = [(Number(i) * SCALE * 7) + pos[0] - 320,
                    (Number(j) * SCALE * 4) + pos[1] - 120, 1,
                    /* (i * 0.01) + (j * 0.01)+ j*20*/];
                parent.addChild(sprite);
                if (indexDeep > 0)
                {
                    nodesDeep[indexDeep] = sprite;
                }

                this.createAnim(sprite);
                const a0 = new DelayTime(0.5 + Math.random() * 0.5);

                indexAnimation = (indexAnimation + 1) % animations.length;
                sprite.runAnimation(new Sequence([a0, this.createAnimSpriteSheet(animations[indexAnimation])]));
            }
        }


        // this.createTween([0, -300, -100])
        // this.createTween([0, -200, -100])
        // this.createTween([0, -100, -100])
        // this.createTween([0, 0, -100])
    }

    createTween(position)
    {
        let count = 0;
        const precision = 0.1;
        const text = new Text('count');
        const a0 = new DelayTime(0, 16);
        const a1 = new CallFunction(() =>
        {
            count = count + precision;
            text.setString(`CZK: ${count}`);
            const seq = new Sequence([a0, a1]);

            text.runAnimation(seq);
        });
        const seq = new Sequence([a0, a1]);

        text.runAnimation(seq);
        text.position = position;
        scene.addChild(text);
    }

    createFPSMetter()
    {
        const text = new Text('FPS');
        const a0 = new DelayTime(0, 16);
        const a1 = new CallFunction(() =>
        {
            text.setString(`fps: ${fpsMeter.currentFps}`);
            const seq = new Sequence([a0, a1]);

            text.runAnimation(seq);
        });
        const seq = new Sequence([a0, a1]);

        text.runAnimation(seq);
        text.position = [-300, -300, -100];
        scene.addChild(text);
    }

    createAnimMove(x = 1, y = 1, duration = 0.5)
    {
        const a0 = new MoveBy(duration, vec2.fromValues(x, -y));
        const a2 = new MoveBy(duration, vec2.fromValues(-x, y));

        return new Sequence([a0, a2]);
    }

    createAnimScale(duration)
    {
        const a0 = new Scale2DBy(duration, vec2.fromValues(-SCALE_ANIMATION, -SCALE_ANIMATION));
        const a2 = new Scale2DBy(duration, vec2.fromValues(SCALE_ANIMATION, SCALE_ANIMATION));

        return new Sequence([a0, a2]);
    }

    createAnimCallFucntion(duration, fnc)
    {
        const a0 = new DelayTime(duration * 2);
        const a1 = new CallFunction(fnc);

        return new Sequence([a0, a1]);
    }

    createAnimSpriteSheet(animation)
    {
        const count = animation.end - animation.start;
        const durationPerFrame = animation.duration / count;
        const spriteSheetAnimationList = [];

        for (let i = animation.start; i < animation.end; i++)
        {
            spriteSheetAnimationList.push(/*textureManager.getTexture(*/`${animation.name}${i}.png`/*)*/);
        }

        return new SpriteSheetAnimation(durationPerFrame, spriteSheetAnimationList, true);
    }

    getAnim(fnc)
    {
        const x = (Math.random() * 10 - 5) * (100 / SCALE);
        const y = (Math.random() * 5 - 2.5) * (100 / SCALE);
        const duration = 4 + Math.random() * 2;
        const seq1 = this.createAnimMove(x, y, duration);
        const seq2 = this.createAnimScale(duration);
        const seq3 = this.createAnimCallFucntion(duration, fnc);

        return {
            seq1,
            seq2,
            seq3, /* , seq4*/
        };
    }

    createAnim(node)
    {
        const fnc = () =>
        {
            const { seq1, seq2, seq3 } = this.getAnim(fnc);

            node.runAnimation(seq1);
            node.runAnimation(seq2);
            node.runAnimation(seq3);
        };

        const { seq1, seq2, seq3 } = this.getAnim(fnc);

        node.runAnimation(seq1);
        node.runAnimation(seq2);
        node.runAnimation(seq3);
    }

    // static TYPE_CHARACTER = {
    //     TEXTURE: "bitmapData",
    //     SPRITE: "shape"
    // };

    async loadCharacters(characters)
    {
        // debugger;

    }

    async loadSceneFromJson(scene)
    {
        const characters = await this.loadCharacters(scene.characters);
        // this.sceneManager.loadSceneFromJson(json);
    }
}
